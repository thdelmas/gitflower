# gitflower

How to not shit where you sleep.

## Man

- Each branch should be merge on it's parent.
- Merge parent on EVERY of it's childs.
- Before pushing/merging on master, stg or dev: Unit tests MUST succed

|  Branch \ Usage | Purpose               | From / To  | Notes |
|-----------------|-----------------------|------------|:-----:|
| master          | Always stable         | None       |       |
| stg             | Testing with prod env | master     |       |
| dev             | Working Branch        | stg        |       |
| feat-           | Adding new feature    | dev        |       |
| fix-            | Fix something         | dev        |       |
| docs-           | Edit documentation    | dev        |       |
| other-          | Edit documentation    | dev        |       |
